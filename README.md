## Reign Test Project

<hr/>
<br/>

### Requirements

* git
* docker
* docker-compose
* ports: 8081, 3000, 27017, 4200

<br/>

### Clone project

`git clone git@gitlab.com:hectorlemus/reign-challenge.git`

<br/>

### Run project (terminal)
`cd reign-challenge`  
`docker-compose up --build -d`

<br/>

### View containers 
If everything goes well you will be able to see the following containers:  

![containers](docs/postman-2.png)

<br/>


### Populate the DB for the first time.

##### CURL
``curl -X POST http://localhost:3000/api/news/consult``

or

##### POSTMAN

![populate-post](docs/postman-1.png)

<br/>

### View web application 

The web application in available on 
[http://localhost:4200](http://localhost:4200)

!['web'](docs/postman-3.png)

<br/>
<hr/>
Thanks for your time, have a nice day.