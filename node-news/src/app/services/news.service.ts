import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HideHitDto } from 'src/app/dto/hide-hit';

const  API = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  constructor(private http: HttpClient ) {}

  public allNews(): Observable<any> {
    return this.http.get(`${API}/news/`);
  }

  public deleteNewsItem(hideHitDto: HideHitDto): Observable<any> {
    return this.http.post(`${API}/news/hide`, hideHitDto);
  }

}
