import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
  name: 'dateAgo',
})
export class DateAgoPipe extends DatePipe  implements PipeTransform {
  transform(value: any, args?: any): any {
    if (value) {
      const now = new Date(Date.now());
      const date = new Date(new Date(value).getTime() + (new Date(value).getTimezoneOffset() * 60000));
      const seconds = Math.floor((+now - +date) / 1000);
      const diffDays = now.getDate() - date.getDate();
      const diffYears = now.getFullYear() - date.getFullYear();

      if (Number.isNaN(seconds)) {
        return '';
      } else if (seconds >= 0 && seconds <= 30) {
        return 'Just now';
      } else if (diffDays === 0) {
        return super.transform(date, "h:mm a");
      } else if (diffYears === 0 && diffDays === 1) {
        return "Yesterday";
      } else {
        return super.transform(date, "MMM d, y");
      }
    }
    return value;
  }
}
