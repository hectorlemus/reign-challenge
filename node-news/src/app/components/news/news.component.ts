import { Component, OnInit } from '@angular/core';
import { HideHitDto } from 'src/app/dto/hide-hit';
import { NewsService } from 'src/app/services/news.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {
  news = [];

  constructor(private newsService: NewsService) {}

  ngOnInit(): void {
    this.loadNews();
  }

  private loadNews(): void {
    this.newsService.allNews()
    .subscribe(data => this.news = data, (error) => console.log(error));
  }

  delete(id: string, index: number): void {
    this.news.splice(index, 1);
    const hideHitDto = new HideHitDto(id);
    this.newsService.deleteNewsItem(hideHitDto)
    .subscribe(() => {}, (error) => console.log(error));
  }

  openNewsItem(url: string): void {
    if (url) {
      window.open(url, '_blank');
    }
  }

}
