import { Body, Controller, Get, HttpException, HttpStatus, Post } from '@nestjs/common';
import { HideHitDto } from 'src/news/dto/hide-hit.dto';
import { Hit } from 'src/news/entity/hit';
import { NewsService } from 'src/news/service/news/news.service';
import { TasksService } from 'src/news/service/tasks/tasks.service';

@Controller('api/news')
export class NewsController {

  constructor(
    private readonly newsService: NewsService,
    private readonly tasksService: TasksService,
  ){}

  @Get()
  async findAll(): Promise<Hit[]> {
    return await this.newsService.findAll();
  }

  @Post('hide')
  async hideHit(@Body() hideHitDto: HideHitDto) {
    await this.newsService.hideHit(hideHitDto.id).then().catch((e) => {
      throw new HttpException({
        status: HttpStatus.BAD_REQUEST,
        error: `${e}`,
      }, HttpStatus.BAD_REQUEST);
    });
  }

  @Post('consult')
  consult(): string {
    try {
      this.tasksService.getHits();
      return 'ok'
    } catch (e) {
      throw new HttpException({
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        error: `${e}`,
      }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
