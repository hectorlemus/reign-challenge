import { HttpModule, Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { Hit, HitSchema } from './entity/hit';
import { NewsController } from './controller/news.controller';
import { NewsService } from './service/news/news.service';
import { TasksService } from './service/tasks/tasks.service';

@Module({
  imports: [
    HttpModule,
    MongooseModule.forFeature([{ name: Hit.name, schema: HitSchema }])
  ],
  controllers: [
    NewsController
  ],
  providers: [
    NewsService,
    TasksService,
    ConfigService,
  ]
})
export class NewsModule {}
