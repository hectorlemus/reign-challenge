import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Hit, HitDocument } from 'src/news/entity/hit';

@Injectable()
export class NewsService {
  constructor(@InjectModel(Hit.name) private readonly hitModel: Model<HitDocument>) {}

  async findAll(): Promise<Hit[]> {
    return this.hitModel.find({"visible" : true}).sort({created_at: 'desc'}).exec();
  }

  async hideHit(id: string) {
    await this.hitModel.findByIdAndUpdate(id, {$set: {'visible': false}})    
  }
}
