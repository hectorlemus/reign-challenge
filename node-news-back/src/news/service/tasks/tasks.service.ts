import { HttpService, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectModel } from '@nestjs/mongoose';
import { Cron } from '@nestjs/schedule';
import { Model } from 'mongoose';
import { Hit, HitDocument } from 'src/news/entity/hit';

@Injectable()
export class TasksService {
  private readonly logger = new Logger(TasksService.name);

  constructor(
    @InjectModel(Hit.name) private readonly hitModel: Model<HitDocument>,
    private readonly configService: ConfigService,
    private readonly httpService: HttpService,
  ) {}
  
  @Cron('0 */1 * * *')
  async handleCron() {
    this.getHits();
  }

  public getHits(): void {
    this.logger.debug('Consulting news');
    const url = this.configService.get<string>('HITS_API');
    if (url) {
      this.httpService.get(url).subscribe(response => {
        this.saveHits(response.data?.hits);
      });
    }
  }

  private async saveHits(hits: Hit[]) {
    if(hits) {
      const last = await this.getLastHit();
      hits.forEach(e => this.saveHit(last, e));
    }
  }

  private async getLastHit(): Promise<HitDocument> {
    return await this.hitModel.findOne({}, {}, { sort: { 'created_at' : -1 }});
  }

  private saveHit(last: Hit, hit: Hit): void {
    if (last == null || hit.story_id > last.story_id) {
      hit.visible = true;
      const hitModel = new this.hitModel(hit);
      hitModel.save();
    }
  }

}
